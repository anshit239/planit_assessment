import randomUserDetails from '../../support/randomDetails'
const errorMessagesContact = require('../../fixtures/errorMessagesContact.json')
const { errMessageEmptyFields, errMessageForname, errMessageEmail, errMessageMessage, infoMessage } = errorMessagesContact
let mandatoryFields = ['forename', 'email', 'message']
describe("TestCase1: Contact page > Validating Error visibility > Submitting empty form", () => {
    beforeEach(() => {
        Cypress.Cookies.preserveOnce("session", "remember_token");
    });
    before(() => {
        cy.viewport(1280, 800);
        cy.visit("/");
        cy.url().should('include', 'planittesting')
    });
    it('Navigates to Contact Page, and submit without filling any fields', () => {
        cy.navigateToTab('contact')
        cy.get('.btn-contact')
            .contains('Submit')
            .click({ force: true })
    });
    it('Validates the error displayed, when user submits an empty form', () => {
        mandatoryFields.forEach(element => {
            cy.get('.control-label[for = ' + element + ']')
                .children()
                .should('have.class', 'req')
                .and('contain', '*')
                .and('have.css', 'color', 'rgb(255, 0, 0)')
        }) //checks that the "*" is shown next to mandatory fields
        cy.get(".alert-error")
            .should('be.visible')
            .and('have.css', 'color', 'rgb(185, 74, 72)')
            .invoke("text")
            .then((errMessage) => {
                expect(errMessage.trim()).to.contain(errMessageEmptyFields.toString())
            });
        cy.get('#forename-err')
            .should('be.visible')
            .and('have.css', 'color', 'rgb(185, 74, 72)')
            .invoke("text")
            .then((errMessage) => {
                expect(errMessage.trim()).to.contain(errMessageForname.toString())
            });
        cy.get('#email-err')
            .should('be.visible')
            .and('have.css', 'color', 'rgb(185, 74, 72)')
            .invoke("text")
            .then((errMessage) => {
                expect(errMessage.trim()).to.contain(errMessageEmail.toString())
            });
        cy.get('#message-err')
            .should('be.visible')
            .and('have.css', 'color', 'rgb(185, 74, 72)')
            .invoke("text")
            .then((errMessage) => {
                expect(errMessage.trim()).to.contain(errMessageMessage.toString())
            });
        cy.get('#telephone-err')
            .should('not.exist')
    });
    it('Fills in all the mandatory fields, and asserts that there are no error displayed', () => {
        for (let index = 0; index < mandatoryFields.length; index++) {
            const element = mandatoryFields[index];
            if (element === "forename") {
                cy.get('#' + element)
                    .type(randomUserDetails[0].firstName)
            } else if (element === "email") {
                cy.get('#' + element)
                    .type(randomUserDetails[0].email)
            } else {
                cy.get('#' + element)
                    .type(randomUserDetails[0].message)
            }
        }
        cy.get(".alert-error")
            .should('not.exist')
        mandatoryFields.forEach(element => {
            cy.get('#' + element + '-err')
                .should('not.exist')
        });
        cy.get('.alert-info')
            .should('be.visible')
            .and('have.css', 'color', 'rgb(58, 135, 173)')
            .invoke("text")
            .then((infoMessage) => {
                expect(infoMessage.trim()).to.contain(infoMessage.toString().trim())
            });
    });
});
