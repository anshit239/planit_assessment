let product1Name = 'Funny Cow'
let product1Quantity = 2
let product1ActualPrice = 10.99
let product1Total = product1Quantity * product1ActualPrice
let product2Name = 'Fluffy Bunny'
let product2Quantity = 1
let product2ActualPrice = 9.99
let FinalcartCount = product1Quantity + product2Quantity
let product2Total = product2Quantity * product2ActualPrice
let Subtotal = product1Total + product2Total
describe("TestCase4: Shop page > Add items in Cart > Validate Cart", () => {
    beforeEach(() => {
        Cypress.Cookies.preserveOnce("session", "remember_token");
    });
    before(() => {
        cy.viewport(1280, 800);
        cy.visit("/");
        cy.url().should('include', 'planittesting')
    });
    it('Navigates to shop Page, and adds items in cart and asserts cart count', () => {
        cy.navigateToTab('shop')
        Cypress._.times(product1Quantity, () => {
            cy.get('div:contains(' + product1Name + ') > p:contains($' + product1ActualPrice + ') > .btn-success')
                .contains('Buy')
                .click({ force: true })
        })
        cy.cartCount(product1Quantity) //asserts the cart count
        cy.get('div:contains(Fluffy Bunny) > p:contains($9.99) > .btn-success')
            .contains('Buy')
            .click({ force: true })
        cy.cartCount(FinalcartCount)
    });
    it('Navigates to cart Page, and verify items in cart', () => {
        cy.navigateToTab('cart')
        //Asserting Funny Cow Data entry
        cy.get('.cart-item:eq(0) > :nth-child(1)').each(($el, index, $list) => {
            var text = $el.text()
            if (text.includes(product1Name)) {
                cy.get('.cart-item:eq(0) > :nth-child(2)').eq(index).then(function (price) {
                    var actualPrice = price.text()
                    expect(actualPrice.trim()).to.equal('$' + product1ActualPrice)
                })
                cy.get('.cart-item:eq(0) > :nth-child(3) > .input-mini')
                    .should('have.value', product1Quantity)
                cy.get('.cart-item:eq(0) > :nth-child(4)').eq(index).then(function (price) {
                    var totalPrice = price.text()
                    expect(totalPrice.trim()).to.equal('$' + product1Total)
                })
            }
        })
        //Asserting Fluffy Bunny Data entry
        cy.get('.cart-item:eq(1) > :nth-child(1)').each(($el, index, $list) => {
            var text = $el.text()
            if (text.includes(product2Name)) {
                cy.get('.cart-item:eq(1) > :nth-child(2)').eq(index).then(function (price) {
                    var actualPrice = price.text()
                    expect(actualPrice.trim()).to.equal('$' + product2ActualPrice)
                })
                cy.get('.cart-item:eq(1) > :nth-child(3) > .input-mini')
                    .should('have.value', product2Quantity)
                cy.get('.cart-item:eq(1) > :nth-child(4)').eq(index).then(function (price) {
                    var totalPrice = price.text()
                    expect(totalPrice.trim()).to.equal('$' + product2Total)
                })
            }
        })
        cy.get(".total")
            .should(($total) => {
                expect($total).to.contain('Total: ' + Subtotal)
            })
    });
});