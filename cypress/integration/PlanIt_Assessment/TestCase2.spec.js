import randomUserDetails from '../../support/randomDetails'
let mandatoryFields = ['forename', 'email', 'message']
describe("TestCase2: Contact page > Submitting form successfully", () => {
    beforeEach(() => {
        Cypress.Cookies.preserveOnce("session", "remember_token");
    });
    before(() => {
        cy.viewport(1280, 800);
        cy.visit("/");
        cy.url().should('include', 'planittesting')
    });
    it('Navigates to Contact Page, and Populate all mandatory fields and submit', () => {
        cy.navigateToTab('contact')
        cy.wait(500)
        mandatoryFields.forEach(element => {
            cy.get('.control-label[for = ' + element + ']')
                .children()
                .should('have.class', 'req')
                .and('contain', '*')
                .and('have.css', 'color', 'rgb(255, 0, 0)')
        }) //checks that the "*" is shown next to mandatory fields
        for (let index = 0; index < mandatoryFields.length; index++) {
            const element = mandatoryFields[index];
            if (element === "forename") {
                cy.get('#' + element)
                    .type(randomUserDetails[0].firstName, { force: true })
            } else if (element === "email") {
                cy.get('#' + element)
                    .type(randomUserDetails[0].email, { force: true })
            } else {
                cy.get('#' + element)
                    .type(randomUserDetails[0].message)
            }
        }
        cy.get('.btn-contact')
            .contains('Submit')
            .click({ force: true })
        cy.get('.modal-header')
            .should('be.visible')
            .and('contain', 'Sending Feedback')
        cy.wait(2000)
        cy.get(".alert-success", { timeout: 10000 })
            .should('be.visible')
            .and('have.css', 'color', 'rgb(70, 136, 71)')
            .and('have.css', 'background-color', 'rgb(223, 240, 216)')
            .invoke("text")
            .then((sucMessage) => {
                expect(sucMessage.trim()).to.contain('Thanks ' + randomUserDetails[0].firstName + ', we appreciate your feedback.'.toString())
            });
    });
});