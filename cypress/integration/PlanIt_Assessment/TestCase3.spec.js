import { formData } from "../../support/Scenario3Env"
const errorMessagesContact = require('../../fixtures/errorMessagesContact.json')
const { errMessageEmptyFields, errMessageInvalidEmail, errMessageInvalidTelNum } = errorMessagesContact
let formFields = ['forename', 'email', 'telephone', 'message']
describe("TestCase3: Contact page > Validating Error's > Submitting invalid data in fields", () => {
    beforeEach(() => {
        Cypress.Cookies.preserveOnce("session", "remember_token");
    });
    before(() => {
        cy.viewport(1280, 800);
        cy.visit("/");
        cy.url().should('include', 'planittesting')
    });
    it('Navigates to Contact Page, and fill-in invalid data in form', () => {
        cy.navigateToTab('contact')
        for (let index = 0; index < formFields.length; index++) {
            const element = formFields[index];
            cy.get('#' + element)
                .type(formData[index])
        }
    })
    it('Validates the error displayed, when user inputs invalid data', () => {
        cy.get(".alert-error")
            .should('be.visible')
            .and('have.css', 'color', 'rgb(185, 74, 72)')
            .invoke("text")
            .then((errMessage) => {
                expect(errMessage.trim()).to.contain(errMessageEmptyFields.toString())
            });
        cy.get('#forename-err')
            .should('not.exist')
        cy.get('#email-err')
            .should('be.visible')
            .and('have.css', 'color', 'rgb(185, 74, 72)')
            .invoke("text")
            .then((errMessage) => {
                expect(errMessage.trim()).to.contain(errMessageInvalidEmail.toString())
            });
        cy.get('#telephone-err')
            .should('be.visible')
            .and('have.css', 'color', 'rgb(185, 74, 72)')
            .invoke("text")
            .then((errMessage) => {
                expect(errMessage.trim()).to.contain(errMessageInvalidTelNum.toString())
            });
    })
})