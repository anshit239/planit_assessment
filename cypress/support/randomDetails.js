const faker = require('Faker');
import { internet, name, lorem } from 'faker';
const randomUserDetails = []
for (let i = 1; i <= 10; i++) {
    var userDetailsArray = {};
    const firstName = name.firstName();
    const email = internet.email();
    const message = lorem.sentences();
    userDetailsArray["firstName"] = firstName;
    userDetailsArray["email"] = email;
    userDetailsArray["message"] = message;
    randomUserDetails.push(userDetailsArray)
}
export default randomUserDetails;