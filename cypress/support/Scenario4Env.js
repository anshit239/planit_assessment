var product1Name = 'Funny Cow'
var product1Quantity = 2
var product1ActualPrice = 10.99
var product1Total = product1Quantity * product1ActualPrice

var product2Name = 'Fluffy Bunny'
var product2Quantity = 1
var product2ActualPrice = 9.99
var product2Total = product2Quantity * product2ActualPrice

var Subtotal = product1Total + product2Total

export let product1 = [product1Name, product1Quantity, product1ActualPrice, product1Total]
export let product2 = [product2Name, product2Quantity, product2ActualPrice, product2Total, Subtotal]