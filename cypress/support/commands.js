

Cypress.Commands.add('navigateToTab', (tab) => {
    cy.get('#nav-' + tab + ' > a')
        .contains(tab.charAt(0).toUpperCase() + tab.slice(1))
        .click({ force: true })
})
Cypress.Commands.add('cartCount', (count) => {
    cy.get('.cart-count')
        .invoke("text")
        .then((cartCount) => {
            expect(cartCount.trim()).to.contain(count)
        });
})